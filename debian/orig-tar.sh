#!/bin/sh
set -e

VERSION=$2
DIR=bintray-client-java-${VERSION}
TAR=../bintray-client-java_${VERSION}.orig.tar.xz

tar -xf $3
rm $3
XZ_OPT=--best tar -c -v -J -f $TAR \
    --exclude 'gradlew*' \
    --exclude 'gradle/wrapper' \
    --exclude '*.jar' \
    $DIR
rm -Rf $DIR
